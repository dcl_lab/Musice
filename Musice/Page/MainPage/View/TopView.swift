//
//  TopView.swift
//  Musice
//
//  Created by cl d on 2022/4/9.
//

import UIKit
import SnapKit

class TopView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setupUI() {
        self.addSubview(topViewLabel)
        self.addSubview(searchImg)
        topViewLabel.sizeToFit()
        topViewLabel.snp.makeConstraints { make in
            make.left.equalToSuperview()
            make.centerY.equalToSuperview()
        }
        
        
        searchImg.snp.makeConstraints { make in
            make.width.height.equalTo(24)
            make.right.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }
    
    
    lazy var topViewLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.text = "Listen Now"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 24)
        return label
    }()
    
    lazy var searchImg: UIImageView = {
        let imgView = UIImageView(image: UIImage(named: "search"))
        return imgView
    }()

}

//
//  TableHeaderView.swift
//  Musice
//
//  Created by cl d on 2022/4/24.
//

import UIKit

class TableHeaderView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .black
        self.setupUI()
    }
    
    func updateHeader(name: String, more: String) {
        self.headerNameLabel.text = name
        self.moreLabel.text = more
    }
    
    func setupUI() {
        self.addSubview(headerNameLabel)
        self.addSubview(moreLabel)
        headerNameLabel.sizeToFit()
        moreLabel.sizeToFit()
        
        headerNameLabel.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(24)
            make.top.bottom.equalToSuperview()
        }
        
        moreLabel.snp.makeConstraints { make in
            make.right.equalToSuperview().inset(24)
            make.top.bottom.equalToSuperview()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    lazy var headerNameLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 22)
        return label
    }()
    
    lazy var moreLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    
}

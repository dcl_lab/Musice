//
//  SliderTableViewCell.swift
//  Musice
//
//  Created by cl d on 2022/4/24.
//

import UIKit

class SliderTableViewCell: UITableViewCell {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }
    
    func setupUI() {
        self.contentView.addSubview(scrollView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView(frame: self.contentView.frame)
        scrollView.backgroundColor = .blue
        return scrollView
    }()

}

//
//  MainPageViewController.swift
//  Musice
//
//  Created by cl d on 2022/4/9.
//

import UIKit

class MainPageViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        setupUI()

    }
    
    
    func setupUI() {
        self.view.addSubview(topLabel)
        self.view.addSubview(displayTable)
        self.topLabel.snp.makeConstraints { make in
            make.top.equalTo(64)
            make.height.equalTo(56)
            make.left.right.equalToSuperview().inset(24)
        }
        
        self.displayTable.snp.makeConstraints { make in
            make.top.equalTo(self.topLabel.snp.bottom).offset(32)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview().inset(166)
        }
        
    }
        
    
    lazy var topLabel: TopView = {
        let view = TopView(frame: .zero)
        view.backgroundColor = .red
        return view
    }()
    
    
    lazy var displayTable: UITableView = {
        let table = UITableView(frame: .zero)
        table.delegate = self
        table.dataSource = self
        table.register(SliderTableViewCell.self, forCellReuseIdentifier: "SliderTableViewCell")
        return table
    }()

}

// MARK: table delegate
extension MainPageViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SliderTableViewCell", for: indexPath) as? SliderTableViewCell else { return UITableViewCell() }
        cell.backgroundColor = .yellow
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 26
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        debugPrint("current section: \(section)")
        
        let view = TableHeaderView()
        view.updateHeader(name: "New Ablums", more: "view all")
        
        return view
    }
    
    
    
}
